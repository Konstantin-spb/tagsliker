create table LIKERS
(
    ID                   INTEGER auto_increment
        primary key,
    CRON                 VARCHAR(255),
    END_PAUSE_INTERVAL   BIGINT       not null,
    MAX_NUMBER_OF_LIKES  INTEGER      not null,
    PASSWORD             VARCHAR(255) not null,
    START_PAUSE_INTERVAL BIGINT       not null,
    USERNAME             VARCHAR(255) not null
--     PROXY_ID             INTEGER,
);


-- alter table LIKERS add  USERNAME             VARCHAR(255);
-- alter table LIKERS add  PASSWORD             VARCHAR(255);
-- alter table LIKERS add  START_PAUSE_INTERVAL BIGINT;
-- alter table LIKERS add  END_PAUSE_INTERVAL   BIGINT;
-- alter table LIKERS add CRON VARCHAR(255);
-- alter table LIKERS add  MAX_NUMBER_OF_LIKES  INTEGER;
-- alter table LIKERS add  PROXY_ID             INTEGER;

create table INSTAGRAM_USERS
(
    USER_ID       BIGINT,
    LIKER_ID    INT,
    is_used boolean not null
);

create table PROXIES
(
    ID         INTEGER auto_increment primary key,
    IP_ADDRESS VARCHAR(255),
    LOGIN      VARCHAR(255),
    PASSWORD   VARCHAR(255),
    PORT       INTEGER,
    SCHEME     VARCHAR(255),
    LIKER_ID   INTEGER not null,
);

create table TAGS
(
    ID       BIGINT auto_increment primary key,
    TAG_NAME VARCHAR(255) not null unique,
    LIKER_ID INTEGER,
);

alter table TAGS
    add constraint TAGS_LIKERS_ID_fk
        foreign key (LIKER_ID) references LIKERS
            on update cascade on delete cascade;

alter table PROXIES
    add constraint PROXIES_LIKERS_ID_fk
        foreign key (LIKER_ID) references LIKERS
            on update cascade on delete cascade;

alter table INSTAGRAM_USERS
    add constraint INSTAGRAM_USERS_LIKERS_ID_fk
        foreign key (LIKER_ID) references LIKERS
            on update cascade on delete cascade;
