package ru.konstantin.repositories

import org.springframework.data.jpa.repository.JpaRepository
import ru.konstantin.models.testEntities.ProxyEntity

interface ProxyEntityRepository: JpaRepository<ProxyEntity, Int> {
}