package ru.konstantin.repositories

import com.sun.xml.internal.bind.v2.runtime.unmarshaller.TagName
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import ru.konstantin.models.Liker
import ru.konstantin.models.Tag

@Repository
interface TagRepository: JpaRepository<Tag, Long> {
    fun getTagByLikerrrAndTagName(liker: Liker, tagName: String): Tag?
}