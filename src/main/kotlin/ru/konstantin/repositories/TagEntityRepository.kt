package ru.konstantin.repositories

import org.springframework.data.jpa.repository.JpaRepository
import ru.konstantin.models.testEntities.TagEntity

interface TagEntityRepository: JpaRepository<TagEntity, Long> {
}