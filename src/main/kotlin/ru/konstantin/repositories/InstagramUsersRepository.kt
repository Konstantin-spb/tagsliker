package ru.konstantin.repositories

import org.springframework.data.jpa.repository.JpaRepository
import ru.konstantin.models.InstagramUser

interface InstagramUsersRepository: JpaRepository<InstagramUser, Long>