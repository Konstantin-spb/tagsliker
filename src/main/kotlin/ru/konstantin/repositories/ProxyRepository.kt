package ru.konstantin.repositories

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import ru.konstantin.models.Proxy

@Repository
interface ProxyRepository: JpaRepository<Proxy, Int> {
    fun findBySchemeAndIpAndPort(schema: String, ip: String, port: Int): Proxy?
}