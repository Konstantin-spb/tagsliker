package ru.konstantin.repositories

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import ru.konstantin.models.Liker

@Repository
interface LikerRepository: JpaRepository<Liker, Int> {
    fun findByUsername(username: String): Liker?
}