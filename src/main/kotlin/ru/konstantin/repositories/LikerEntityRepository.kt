package ru.konstantin.repositories

import org.springframework.data.jpa.repository.JpaRepository
import ru.konstantin.models.testEntities.LikerEntity

interface LikerEntityRepository: JpaRepository<LikerEntity, Int> {
}