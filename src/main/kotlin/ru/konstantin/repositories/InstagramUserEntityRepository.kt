package ru.konstantin.repositories

import org.springframework.data.jpa.repository.JpaRepository
import ru.konstantin.models.testEntities.InstagramUserEntity

interface InstagramUserEntityRepository: JpaRepository<InstagramUserEntity, Int> {
}