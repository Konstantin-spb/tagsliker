package ru.konstantin

import org.apache.http.impl.client.CloseableHttpClient
import sun.net.www.http.HttpClient

interface InstagramCapabilities {

    fun signIn()

    fun getUserPage(accountName: String, httpClient: CloseableHttpClient)

    fun getPostsLinks(userPageSource: String): List<String>

    fun getPostComments(postLinkShortCode: String): List<String>

    fun likeComment(): Boolean
}