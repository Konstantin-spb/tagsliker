package ru.konstantin.models

import javax.persistence.*

@Entity
@Table(name = "tags", uniqueConstraints=[UniqueConstraint(columnNames=["liker_id", "tag_name"])])
data class Tag(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = -1,

    @Column(name = "tag_name", nullable = false)
    var tagName: String = "",

    @ManyToOne
    @JoinColumn(name = "liker_id")
    var likerrr: Liker = Liker()
//    @ManyToMany(mappedBy = "tags")
//    var likers: Set<Liker> = mutableSetOf()
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Tag

        if (id != other.id) return false
        if (tagName != other.tagName) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + tagName.hashCode()
        return result
    }
}