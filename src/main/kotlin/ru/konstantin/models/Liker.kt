package ru.konstantin.models

import javax.persistence.*
import javax.persistence.FetchType



@Entity
@Table(name = "likers")
data class Liker(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int = -1,

//    val userId: Long,//id в принципе не нужен так как логин осуществляется по имени и оно уникально

    @Column(nullable = false/*, unique = true*/)
    var username: String = "",

    @Column(nullable = false)
    var password: String = "",

    @Column(nullable = false)
    var startPauseInterval: Long = 0,

    @Column(nullable = false)
    var endPauseInterval: Long = 0,

    @Column(nullable = false)
    var maxNumberOfLikes: Int = 1600,

    @Column
    var cron: String = "0 30 7 * * *",

//    @ManyToOne(fetch = FetchType.EAGER, cascade = [CascadeType.ALL])
    @OneToOne(cascade = [CascadeType.ALL])
    @JoinColumn(name = "proxy_id")
    var proxy: Proxy? = null,

    @OneToMany(mappedBy = "likerrr", cascade = [CascadeType.ALL], fetch = FetchType.EAGER/*, orphanRemoval = true*/)
    var tags: MutableSet<Tag> = mutableSetOf(),

    @OneToMany(mappedBy = "liker", cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
    var instagramUsers: MutableSet<InstagramUser> = mutableSetOf()

)