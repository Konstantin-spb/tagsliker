package ru.konstantin.models.testEntities

import javax.persistence.*

@Entity
@Table(name = "TAGS", schema = "PUBLIC", catalog = "DATABASE")
open class TagEntity {
    @get:Id
    @get:Column(name = "ID", nullable = false)
    var ID: Long? = null
    @get:Basic
    @get:Column(name = "TAG_NAME", nullable = false)
    var TAGNAME: String? = null
    @get:Basic
    @get:Column(name = "LIKER_ID", nullable = true, insertable = false, updatable = false)
    var LIKERID: Int? = null

    @get:ManyToOne(fetch = FetchType.LAZY)
    @get:JoinColumn(name = "LIKER_ID", referencedColumnName = "ID")
    var refLikerEntity: LikerEntity? = null

    override fun toString(): String =
        "Entity of type: ${javaClass.name} ( " +
                "ID = $ID " +
                "TAGNAME = $TAGNAME " +
                "LIKERID = $LIKERID " +
                ")"

    // constant value returned to avoid entity inequality to itself before and after it's update/merge
    override fun hashCode(): Int = 42

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as TagEntity

        if (ID != other.ID) return false
        if (TAGNAME != other.TAGNAME) return false
        if (LIKERID != other.LIKERID) return false

        return true
    }

}

