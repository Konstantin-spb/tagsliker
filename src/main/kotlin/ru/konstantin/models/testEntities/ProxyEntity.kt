package ru.konstantin.models.testEntities

import javax.persistence.*

@Entity
@Table(name = "PROXIES", schema = "PUBLIC", catalog = "DATABASE")
open class ProxyEntity {
    @get:Id
    @get:Column(name = "ID", nullable = false)
    var ID: Int? = null
    @get:Basic
    @get:Column(name = "IP_ADDRESS", nullable = true)
    var IPADDRESS: String? = null
    @get:Basic
    @get:Column(name = "LOGIN", nullable = true)
    var LOGIN: String? = null
    @get:Basic
    @get:Column(name = "PASSWORD", nullable = true)
    var PASSWORD: String? = null
    @get:Basic
    @get:Column(name = "PORT", nullable = true)
    var PORT: Int? = null
    @get:Basic
    @get:Column(name = "SCHEME", nullable = true)
    var SCHEME: String? = null
    @get:Basic
    @get:Column(name = "LIKER_ID", nullable = false, insertable = false, updatable = false)
    var LIKERID: Int? = null

    @get:ManyToOne(fetch = FetchType.LAZY)
    @get:JoinColumn(name = "LIKER_ID", referencedColumnName = "ID")
    var refLikerEntity: LikerEntity? = null

    override fun toString(): String =
        "Entity of type: ${javaClass.name} ( " +
                "ID = $ID " +
                "IPADDRESS = $IPADDRESS " +
                "LOGIN = $LOGIN " +
                "PASSWORD = $PASSWORD " +
                "PORT = $PORT " +
                "SCHEME = $SCHEME " +
                "LIKERID = $LIKERID " +
                ")"

    // constant value returned to avoid entity inequality to itself before and after it's update/merge
    override fun hashCode(): Int = 42

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as ProxyEntity

        if (ID != other.ID) return false
        if (IPADDRESS != other.IPADDRESS) return false
        if (LOGIN != other.LOGIN) return false
        if (PASSWORD != other.PASSWORD) return false
        if (PORT != other.PORT) return false
        if (SCHEME != other.SCHEME) return false
        if (LIKERID != other.LIKERID) return false

        return true
    }
}

