package ru.konstantin.models.testEntities

import javax.persistence.*

@Entity
@Table(name = "INSTAGRAM_USERS", schema = "PUBLIC", catalog = "DATABASE")
open class InstagramUserEntity {
    @get:Id
    @get:Column(name = "ID", nullable = false, insertable = false, updatable = false)
    var ID: Int? = null
    @get:Basic
    @get:Column(name = "USER_ID", nullable = true)
    var USERID: Long? = null
    @get:Basic
    @get:Column(name = "LIKER_ID", nullable = true, insertable = false, updatable = false)
    var LIKERID: Int? = null
    @get:Basic
    @get:Column(name = "IS_USED", nullable = false)
    var ISUSED: Boolean? = null

    @get:ManyToOne(fetch = FetchType.LAZY)
    @get:JoinColumn(name = "LIKER_ID", referencedColumnName = "ID")
    var refLikerEntity: LikerEntity? = null

    override fun toString(): String =
        "Entity of type: ${javaClass.name} ( " +
                "USERID = $USERID " +
                "LIKERID = $LIKERID " +
                "ISUSED = $ISUSED " +
                ")"

    // constant value returned to avoid entity inequality to itself before and after it's update/merge
    override fun hashCode(): Int = 42

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as InstagramUserEntity

        if (USERID != other.USERID) return false
        if (LIKERID != other.LIKERID) return false
        if (ISUSED != other.ISUSED) return false

        return true
    }

}

