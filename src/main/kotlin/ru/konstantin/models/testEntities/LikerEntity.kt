package ru.konstantin.models.testEntities

import javax.persistence.*

@Entity
@Table(name = "LIKERS", schema = "PUBLIC", catalog = "DATABASE")
open class LikerEntity {
    @get:Id
    @get:Column(name = "ID", nullable = false, insertable = false, updatable = false)
    var ID: Int? = null
    @get:Basic
    @get:Column(name = "CRON", nullable = true)
    var CRON: String? = null
    @get:Basic
    @get:Column(name = "END_PAUSE_INTERVAL", nullable = false)
    var ENDPAUSEINTERVAL: Long? = null
    @get:Basic
    @get:Column(name = "MAX_NUMBER_OF_LIKES", nullable = false)
    var MAXNUMBEROFLIKES: Int? = null
    @get:Basic
    @get:Column(name = "PASSWORD", nullable = false)
    var PASSWORD: String? = null
    @get:Basic
    @get:Column(name = "START_PAUSE_INTERVAL", nullable = false)
    var STARTPAUSEINTERVAL: Long? = null
    @get:Basic
    @get:Column(name = "USERNAME", nullable = false)
    var USERNAME: String? = null

    @get:OneToMany(mappedBy = "refLikerEntity")
    var refInstagramUserEntities: List<InstagramUserEntity>? = null
    @get:OneToMany(mappedBy = "refLikerEntity")
    var refProxyEntities: List<ProxyEntity>? = null
    @get:OneToMany(mappedBy = "refLikerEntity")
    var refTagEntities: List<TagEntity>? = null

    override fun toString(): String =
        "Entity of type: ${javaClass.name} ( " +
                "ID = $ID " +
                "CRON = $CRON " +
                "ENDPAUSEINTERVAL = $ENDPAUSEINTERVAL " +
                "MAXNUMBEROFLIKES = $MAXNUMBEROFLIKES " +
                "PASSWORD = $PASSWORD " +
                "STARTPAUSEINTERVAL = $STARTPAUSEINTERVAL " +
                "USERNAME = $USERNAME " +
                ")"

    // constant value returned to avoid entity inequality to itself before and after it's update/merge
    override fun hashCode(): Int = 42

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as LikerEntity

        if (ID != other.ID) return false
        if (CRON != other.CRON) return false
        if (ENDPAUSEINTERVAL != other.ENDPAUSEINTERVAL) return false
        if (MAXNUMBEROFLIKES != other.MAXNUMBEROFLIKES) return false
        if (PASSWORD != other.PASSWORD) return false
        if (STARTPAUSEINTERVAL != other.STARTPAUSEINTERVAL) return false
        if (USERNAME != other.USERNAME) return false

        return true
    }

}

