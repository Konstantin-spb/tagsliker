package ru.konstantin.models

import javax.persistence.*

@Entity
@Table(name = "proxies"/*, uniqueConstraints=[UniqueConstraint(columnNames=["scheme", "ip_address", "port", "login"])]*/)
data class Proxy(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int = -1,

    @Column(name ="scheme")
    var scheme: String = "",

    @Column(name = "ip_address")
    var ip: String = "",

    @Column(name = "port")
    var port: Int = 0,

    @Column(name = "login")
    var login: String = "",

    @Column(name = "password")
    var password: String = "",

    @OneToOne(mappedBy = "proxy")
    var liker: Liker? = null
) {
    override fun toString(): String {
        var str =  ""
        if (scheme.isNotEmpty() && login.isEmpty()) {
            str = "$scheme://$ip:$port"
        } else if (scheme.isNotEmpty() && login.isNotEmpty()) {
            str = "$scheme://$ip:$port@$login:$password"
        }
        return str
    }
}