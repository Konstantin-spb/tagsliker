package ru.konstantin.models.tornadofx

import javafx.scene.layout.AnchorPane
import javafx.scene.layout.HBox
import ru.konstantin.models.InstagramUser

class Liker(
    var id: Int = -1,
    var username: String = "",
    var password: String = "",
    var proxy: String = "",
    var instagramUsers: Set<InstagramUser> = mutableSetOf(),
    var hboxPaneForControlButtons: HBox = HBox(),
    var hboxPaneVerificationCodeField: HBox = HBox()
)