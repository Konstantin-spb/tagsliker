package ru.konstantin.models

import javax.persistence.*

@Entity
@Table(name = "instagram_users")
data class InstagramUser(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

//    @Column(name = "ids", nullable = false)
    var id: Long = -1,

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "liker_id")
    var liker: Liker? = null
//    @ManyToMany(mappedBy = "instagramUsers", fetch = FetchType.EAGER)
//    var likers: Set<Liker> = HashSet()
)