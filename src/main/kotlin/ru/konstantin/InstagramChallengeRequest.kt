package ru.konstantin

import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.http.client.ClientProtocolException
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.ByteArrayEntity
import org.apache.http.entity.ContentType
import org.apache.http.entity.StringEntity
import org.apache.http.entity.mime.MultipartEntityBuilder
import org.apache.http.util.EntityUtils
import org.brunocvcunha.instagram4j.InstagramConstants
import org.brunocvcunha.instagram4j.requests.InstagramPostRequest
import org.brunocvcunha.instagram4j.requests.InstagramRequest
import org.brunocvcunha.instagram4j.requests.payload.InstagramLikeResult
import org.brunocvcunha.instagram4j.util.InstagramHashUtil
import org.slf4j.LoggerFactory
import java.io.IOException
import java.util.LinkedHashMap

//запрос для отправки письма на почту, для входа в эккаунт
class InstagramChallengeRequest(link: String, formData: String): InstagramRequest<InstagramChallengeResult>() {
    override fun getMethod(): String {
        return "POST"
    }

    private var logger = LoggerFactory.getLogger(InstagramChallengeRequest::class.java)

     var formData = ""
    var link = ""
    init {
        this.link = link
        this.formData = formData
    }

    override fun getUrl(): String {

        return link
    }

    override fun parseResult(statusCode: Int, content: String): InstagramChallengeResult {
        logger.info(content)
//        return parseJson(statusCode, content, InstagramChallengeResult::class.java)
        var instagramChallengeResult = InstagramChallengeResult()
        instagramChallengeResult.status = statusCode.toString()
        return instagramChallengeResult
    }

    override fun getPayload(): String {

        val likeMap = LinkedHashMap<String, Any>()
        likeMap["_uuid"] = api.uuid
        likeMap["_uid"] = api.userId
        likeMap["_csrftoken"] = api.orFetchCsrf
//        likeMap["media_id"] = mediaId

        val mapper = ObjectMapper()

        return mapper.writeValueAsString(likeMap)
    }

    @Throws(ClientProtocolException::class, IOException::class)
    override fun execute(): InstagramChallengeResult {
        val post = HttpPost(url)
//        post.addHeader("Connection", "close")
//        post.addHeader("Accept", "*/*")
//        post.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
//        post.addHeader("Cookie2", "\$Version=1")
//        post.addHeader("origin", "https://i.instagram.com")
//        post.addHeader("Accept-Language", "en-US")
//        post.addHeader("User-Agent", InstagramConstants.USER_AGENT)
//        post.addHeader("_uuid", api.uuid)
//        post.addHeader("_uid", api.userId.toString())
//        post.addHeader("_csrftoken", api.orFetchCsrf)

//        val entity = ByteArrayEntity(formData.toByteArray())
        val meb = MultipartEntityBuilder.create()
        val entity = meb.addTextBody(formData.split("=")[0], formData.split("=")[1]).build()
        post.entity = entity
        post.addHeader("origin", "https://i.instagram.com")
        post.addHeader("Host", "www.instagram.com")
        post.addHeader("Content-Type", "application/x-www-form-urlencoded")
        post.addHeader("X-CSRFToken", api.orFetchCsrf)
        post.addHeader("X-Requested-With", "XMLHttpRequest")
        post.addHeader("Accept", "*/*")
        post.addHeader("Accept-Encoding", "gzip, deflate, br")
        post.addHeader("Accept-Language", "en-US,en;q=0.9")
        post.addHeader("Pragma", "no-cache")
        post.addHeader("referer", link)

        logger.debug("User-Agent: " + InstagramConstants.USER_AGENT)
        var payload = payload
        logger.debug("Base Payload: $payload")

        if (isSigned) {
            payload = InstagramHashUtil.generateSignature(payload)
        }
        logger.debug("Final Payload: $payload")
        post.entity = StringEntity(payload)

        val response = api.client.execute(post)
        api.lastResponse = response

        val resultCode = response.statusLine.statusCode
        val content = EntityUtils.toString(response.entity)

        post.releaseConnection()

//        return parseResult(resultCode, content)
        var instagramChallengeResult = InstagramChallengeResult()
        instagramChallengeResult.status = response.statusLine.statusCode.toString()
        return instagramChallengeResult
    }
}