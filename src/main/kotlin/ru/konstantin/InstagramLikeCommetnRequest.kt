package ru.konstantin

import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.log4j.Logger
import org.brunocvcunha.instagram4j.requests.InstagramPostRequest
import org.brunocvcunha.instagram4j.requests.payload.InstagramLikeResult

class InstagramLikeCommentRequest(private val commentId: Long) : InstagramPostRequest<InstagramLikeResult>() {
    private val log = Logger.getLogger(InstagramLikeCommentRequest::class.java)

    override fun getUrl(): String {
//        return "comment/" + this.commentId + "/like/"
        return "media/$commentId/comment_like/"
    }

    override fun getPayload(): String {
        try {
            val likeMap = mutableMapOf<String, Any>()
            likeMap.put("_uuid", this.api.uuid)
            likeMap.put("_uid", this.api.userId)
            likeMap.put("_csrftoken", this.api.orFetchCsrf)
//            likeMap.put("comment_id", this.commentId)
            val mapper = ObjectMapper()
            return mapper.writeValueAsString(likeMap)
        } catch (var4: Throwable) {
            throw var4
        }

    }

    override fun parseResult(statusCode: Int, content: String): InstagramLikeResult {
        try {
            return this.parseJson(statusCode, content, InstagramLikeResult::class.java) as InstagramLikeResult
        } catch (var4: Throwable) {
            throw var4
        }

    }

//    companion object {
//        private val log = Logger.getLogger(ru.konstantin.InstagramLikeCommentRequest::class.java)
//    }
}