package ru.konstantin

data class Comment (

    /**
     * get id of comment on instagram
     *
     * @return String id
     */
    var id: String = "",
    /**
     * get text of comment
     *
     * @return String text
     */
    var text: String = "",
    /**
     * get time of comment in timestamp format
     *
     * @return int timestamp
     */
    var timestamp: Int = 0,
    /**
     * get owner id of comment
     *
     * @return String owner id
     */
    var ownerId: String = "",
    /**
     * get owner profile picture of comment
     *
     * @return String profile picture url
     */
    var ownerProfilePicUrl: String = "",
    /**
     * get owner username of comment
     *
     * @return String username
     */
    var ownerUsername: String = "",
    /**
     * get post short code that this comment for it
     *
     * @return String username
     */
    var postShortCode: String = ""
)
