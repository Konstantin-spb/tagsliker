package ru.konstantin

import java.util.concurrent.ScheduledFuture

class CommonVariables {

    companion object Variables {
        var scheduledFutureMap = mutableMapOf<Int, ScheduledFuture<*>>()
    }
}