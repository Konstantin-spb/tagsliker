package ru.konstantin.tornadofx.views

import javafx.scene.control.Button
import javafx.scene.layout.AnchorPane
import org.slf4j.LoggerFactory
import tornadofx.*

class ExampleAccountEditDialog : View() {
    private var logger = LoggerFactory.getLogger(ExampleAccountEditDialog::class.java)

    override val root: AnchorPane by fxml("/fxml/accountEditDialog.fxml")
    private val cancelButton: Button by fxid("cancelButton")

    fun closeWindow() {
        close()
    }

    override fun onDock() {
        println("accountEditDialog open!")
    }

    override fun onUndock() {
        println("accountEditDialog close!")
    }
//override val root = vbox {
//    button("Go to MyView1") {
//        action {
//            replaceWith<ExampleAccountEditDialog>()
//        }
//    }
//}
}
