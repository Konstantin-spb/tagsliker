package ru.konstantin.tornadofx.views

import javafx.fxml.FXML
import javafx.scene.control.TextArea
import javafx.scene.control.TextField
import javafx.scene.layout.AnchorPane
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import ru.konstantin.repositories.LikerRepository
import ru.konstantin.tornadofx.controllers.AccountEditController
import tornadofx.View


class AccountEditView : View() {
    private var logger = LoggerFactory.getLogger(AccountEditView::class.java)

    override val root: AnchorPane by fxml("/fxml/addNewAccount.fxml")
    private val accountEditController: AccountEditController by inject()
    private val mainView: MainView by inject()

    private val likerRepository: LikerRepository by di()

     val loginTextField: TextField by fxid("loginTextField")
     val passwordTextField: TextField by fxid("passwordTextField")
     val pauseBetweenLikesTextField: TextField by fxid("pauseBetweenLikes")
     val maxNumberOfLikesTextField: TextField by fxid("maxNumberOfLikes")
     val cronTextField: TextField by fxid("cronTextField")
//    private val saveButton: Button by fxid("saveButton")
//    private val cancelButton: Button by fxid("cancelButton")
     val tagsTextArea: TextArea by fxid("tagsTextArea")
     val proxyIpAndPortTextField: TextField by fxid("proxyIpAndPort")
     val proxyLoginTextField: TextField by fxid("proxyLogin")
     val proxyPasswordTextField: TextField by fxid("proxyPassword")

    fun fillTheWindow() {
        GlobalScope.launch(Dispatchers.Main) {
            println("accountEditDialog open!")
            val selectedItem = mainView.mainTable.selectionModel.selectedItems.first()
            if (selectedItem != null) {
                val liker = likerRepository.getOne(selectedItem.id)
                if (liker.id > 0) {
                    loginTextField.text = liker.username
                    passwordTextField.text = liker.password
                    pauseBetweenLikesTextField.text = "${liker.startPauseInterval}..${liker.endPauseInterval}"
                    maxNumberOfLikesTextField.text = liker.maxNumberOfLikes.toString()
                    cronTextField.text = liker.cron
                    if (liker.tags.count() > 0 ) {
                        val tagnameList = liker.tags.map { it.tagName }
                        tagsTextArea.text = tagnameList.joinToString("\n")
                    }
                    logger.info("Account ${liker.username} don't have tags ...")
                    proxyIpAndPortTextField.text = "${liker.proxy?.scheme?:""}://${liker.proxy?.ip?:""}:${liker.proxy?.port?:""}"
                    proxyLoginTextField.text = liker.proxy?.login?:""
                    proxyPasswordTextField.text = liker.proxy?.password?:""
                }
            }
        }
    }

    @FXML
    fun saveAccount() {
        GlobalScope.launch {
            accountEditController.createNewOrEditAccount()
            GlobalScope.launch(Dispatchers.Main) {
                mainView.fillTheTable()
                closeWindow()
            }
        }
    }

    fun openNewWindow() {
        GlobalScope.launch(Dispatchers.Main) {
            openInternalWindow<ExampleAccountEditDialog>()
        }
    }

    fun closeWindow() {
        loginTextField.clear()
        passwordTextField.clear()
        proxyIpAndPortTextField.clear()
        proxyLoginTextField.clear()
        proxyPasswordTextField.clear()
        close()
    }
}