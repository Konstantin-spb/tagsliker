package ru.konstantin.tornadofx.views

import javafx.collections.FXCollections
import javafx.fxml.FXML
import javafx.scene.control.*
import javafx.scene.layout.BorderPane
import javafx.scene.layout.HBox
import javafx.scene.paint.Color
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler
import org.springframework.scheduling.support.PeriodicTrigger
import ru.konstantin.CommonVariables
import ru.konstantin.models.InstagramUser
import ru.konstantin.models.Tag
import ru.konstantin.models.tornadofx.Liker
import ru.konstantin.repositories.LikerRepository
import ru.konstantin.repositories.TagRepository
import ru.konstantin.tornadofx.RunnableTask
import ru.konstantin.tornadofx.controllers.MainController
import tornadofx.View
import tornadofx.action
import tornadofx.toProperty
import tornadofx.value
import kotlin.collections.set

class MainView : View("My View") {
    override val root: BorderPane by fxml("/fxml/mainTable.fxml")

    private var logger = LoggerFactory.getLogger(MainView::class.java)
    private val scheduler: ThreadPoolTaskScheduler by di("MyPool")
    private val accountEditView: AccountEditView by inject()

    private val likerRepository: LikerRepository by di()
    private val tagRepository: TagRepository by di()

//    private var jobs: MutableMap<Int, Job> = mutableMapOf()
    private var startButtonsMap: MutableMap<Int, Button> = mutableMapOf()
//    private var stopButtonsMap: MutableMap<Int, Button> = mutableMapOf()

    private val mainController: MainController by inject()
    private val usernameColumn: TableColumn<Liker, String> by fxid("usernameColumn")
    private val passwordColumn: TableColumn<Liker, String> by fxid("passwordColumn")
    private val proxyColumn: TableColumn<Liker, String> by fxid("proxyColumn")
    private val followsColumn: TableColumn<Liker, String> by fxid("followsColumn")
    private val controlsColumn: TableColumn<Liker, HBox> by fxid("controlsColumn")
    private val verificationCodeColumn: TableColumn<Liker, HBox> by fxid("verificationCodeColumn")

    val mainTable: TableView<Liker> by fxid("usersTable")
    private val addNewUserButton: Button by fxid("addNewUserButton")

    init {
        GlobalScope.launch {
//            val tags = mutableSetOf<Tag>(Tag(tagName = "programing"), Tag(tagName = "job"))
//            val instagramUserSet = mutableSetOf(InstagramUser(), InstagramUser())

//            var liker = ru.konstantin.models.Liker()
//            liker.username = "dfgh"
//            liker.password = "dfgy"
//            liker.startPauseInterval = 14
//            liker.endPauseInterval = 16
//            liker.maxNumberOfLikes = 15
//            liker.cron = "dfghjkl"
//            liker = likerRepository.save(liker)
//            liker = likerRepository.save(liker)
//
//            tags.forEach { it.likerrr = liker }
//            tagRepository.saveAll(tags)
//            println()


//            val likersFromDB = mainController.getAllLikers()
//            val likersFromTornado = mutableListOf<Liker>()
//            likersFromDB.forEach {
//                val oneLikerTornado = Liker()
//                oneLikerTornado.id = it.id
//                oneLikerTornado.username = it.username
//                oneLikerTornado.password = it.password
//                oneLikerTornado.proxy = it.proxy.toString()
//                oneLikerTornado.instagramUsers = it.instagramUsers
//
//                val startButton = Button("Start")
//                startButton.apply {
//                    textFill = Color.RED
//
//                    action {
//                        //                    GlobalScope.launch {
//                        startJob(it)
////                    }
//                        GlobalScope.launch(Dispatchers.Main) {
//                            isDisable = true
//                        }
//                    }
//                }
//                startButtonsMap[it.id] = startButton
////            startButton.text = "dsfew"
//                val stopButton = Button("Stop")
//                stopButton.apply {
//                    textFill = Color.DARKSALMON
//                    id = it.id.toString()
//
//                    action {
//                        //                    GlobalScope.launch {
//                        val scheduledFuture = CommonVariables.scheduledFutureMap[it.id]
//                        println(CommonVariables.scheduledFutureMap)
//                        while (scheduledFuture != null) {
//                            scheduledFuture.cancel(true)
//                            logger.info("Canceling job...")
//                            if (scheduledFuture.isCancelled) {
//                                break
//                            }
//                        }
//
//                        CommonVariables.scheduledFutureMap.remove(it.id)
//                        println("Account with id ${it.id} was stopped.")
//
//                        GlobalScope.launch(Dispatchers.Main) {
//                            startButtonsMap[id.toInt()]?.isDisable = false
//                            println()
//                        }
//                    }
//                }
//
//                val separator = Separator()
//
//                oneLikerTornado.hboxPaneForControlButtons.add(startButton)
//                oneLikerTornado.hboxPaneForControlButtons.add(separator)
//                oneLikerTornado.hboxPaneForControlButtons.add(stopButton)
//                oneLikerTornado.hboxPaneForControlButtons.add(separator)
//
//                val textField = TextField()
//                textField.prefWidth = 100.0
//
//                val sendCode = Button("Send Code")
//                oneLikerTornado.hboxPaneVerificationCodeField.add(textField)
//                oneLikerTornado.hboxPaneVerificationCodeField.add(separator)
//                oneLikerTornado.hboxPaneVerificationCodeField.add(sendCode)
//
//                likersFromTornado.add(oneLikerTornado)
//            }
            fillTheTable()
        }
    }

    fun fillTheTable() {
//        GlobalScope.launch(Dispatchers.Main) {
            //////////////////////////////////////////////////////
            GlobalScope.launch {
                val likersFromDB = mainController.getAllLikers()
//            }
            val likersFromTornado = mutableListOf<Liker>()
            likersFromDB.forEach {
                val oneLikerTornado = Liker()
                oneLikerTornado.id = it.id
                oneLikerTornado.username = it.username
                oneLikerTornado.password = it.password
                oneLikerTornado.proxy = it.proxy.toString()
                oneLikerTornado.instagramUsers = it.instagramUsers

                val startButton = Button("Start")
                startButton.apply {
                    textFill = Color.RED

                    action {
                        //                    GlobalScope.launch {
                        startJob(it)
//                    }
                        GlobalScope.launch(Dispatchers.Main) {
                            isDisable = true
                        }
                    }
                }
                startButtonsMap[it.id] = startButton
//            startButton.text = "dsfew"
                val stopButton = Button("Stop")
                stopButton.apply {
                    textFill = Color.DARKSALMON
                    id = it.id.toString()

                    action {
                        //                    GlobalScope.launch {
                        val scheduledFuture = CommonVariables.scheduledFutureMap[it.id]
                        println(CommonVariables.scheduledFutureMap)
                        while (scheduledFuture != null) {
                            scheduledFuture.cancel(true)
                            logger.info("Canceling job...")
                            if (scheduledFuture.isCancelled) {
                                break
                            }
                        }

                        CommonVariables.scheduledFutureMap.remove(it.id)
//                        println("Account with id ${it.id} was stopped.")

                        GlobalScope.launch(Dispatchers.Main) {
                            startButtonsMap[id.toInt()]?.isDisable = false
                            println()
                        }
                    }
                }

                val separator = Separator()

                oneLikerTornado.hboxPaneForControlButtons.add(startButton)
                oneLikerTornado.hboxPaneForControlButtons.add(separator)
                oneLikerTornado.hboxPaneForControlButtons.add(stopButton)
                oneLikerTornado.hboxPaneForControlButtons.add(separator)

                val textField = TextField()
                textField.prefWidth = 100.0

                val sendCode = Button("Send Code")
                oneLikerTornado.hboxPaneVerificationCodeField.add(textField)
                oneLikerTornado.hboxPaneVerificationCodeField.add(separator)
                oneLikerTornado.hboxPaneVerificationCodeField.add(sendCode)

                likersFromTornado.add(oneLikerTornado)
            }
            //////////////////////////////////////////////////////
            //        val likersList = likerRepository.findAll()
            val data = FXCollections.observableArrayList<Liker>()

            data.addAll(likersFromTornado)
            mainTable.items = data
            usernameColumn.value { it.value.username.toProperty() }
            passwordColumn.value { it.value.password.toProperty() }
            proxyColumn.value { it.value.proxy.toProperty() }
            followsColumn.value { (1..1000000).random().toProperty() }
            controlsColumn.value { it.value.hboxPaneForControlButtons.toProperty() }
            verificationCodeColumn.value { it.value.hboxPaneVerificationCodeField.toProperty() }
            println("Table is updated...")
        }
    }

    fun openNewWindow() {
        GlobalScope.launch(Dispatchers.Main) {
            openInternalWindow<AccountEditView>()
        }
    }

    fun openEditWindow() {
        GlobalScope.launch(Dispatchers.Main) {
            openInternalWindow<AccountEditView>()
            accountEditView.fillTheWindow()
        }
    }

    @FXML
    fun deleteNewAccount() {
        GlobalScope.launch(Dispatchers.Main) {
            val accountId = mainTable.getSelectionModel().getSelectedItem().id
            println("Account with id $accountId was deleted.")
        }
    }

    fun startJob(liker: ru.konstantin.models.Liker) {
//        val job = likerActions.startHere(liker.username, (1000..10000).random().toLong())
//        val scheduledFuture = likerActions.startHere(liker.username, (1000..10000).random().toLong())
        val runnableTask = RunnableTask(liker)
        runnableTask.setParams(liker.username)

        val scheduledFuture = scheduler.schedule(runnableTask, PeriodicTrigger((1000..5000).random().toLong()))
//        jobs[liker.id] = job
        CommonVariables.scheduledFutureMap.put(liker.id, scheduledFuture)
//        println("Account with id ${liker.id} was started.")
    }
}
