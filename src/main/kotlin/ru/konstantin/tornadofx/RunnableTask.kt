package ru.konstantin.tornadofx

//import org.springframework.kotlin.coroutine.annotation.Coroutine
//import org.springframework.kotlin.coroutine.context.DEFAULT_DISPATCHER
import kotlinx.coroutines.*
import org.apache.http.HttpHost
import org.apache.http.auth.AuthScope
import org.apache.http.auth.UsernamePasswordCredentials
import org.apache.http.impl.client.BasicCredentialsProvider
import org.brunocvcunha.instagram4j.Instagram4j
import org.brunocvcunha.instagram4j.requests.InstagramLikeRequest
import org.brunocvcunha.instagram4j.requests.InstagramTagFeedRequest
import org.brunocvcunha.instagram4j.requests.payload.InstagramFeedItem
import org.brunocvcunha.instagram4j.requests.payload.InstagramFeedResult
import org.brunocvcunha.instagram4j.requests.payload.InstagramLoginResult
import org.slf4j.LoggerFactory
import ru.konstantin.InstagramChallengeRequest
import ru.konstantin.models.Liker
import ru.konstantin.models.Tag
import java.io.File
import java.lang.Runnable
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardOpenOption
import java.util.*

//@Coroutine(DEFAULT_DISPATCHER)
//@Component
//@Scope(value = "prototype")
class RunnableTask(liker: Liker) : Runnable {
    private var logger = LoggerFactory.getLogger(RunnableTask::class.java)

    var accountName = ""

    var instaLogin = ""
    var instaPassword = ""
    var startPauseInterval: Long = -1
    var endPauseInterval: Long = -1
    var maxNumberOfLikes: Int = -1
    var tagsSet: Set<Tag>
    var proxyScheme = ""
    var proxyIp = ""
    var proxyPort = -1
    var proxyLogin = ""
    var proxyPassword = ""

    var rotatorCount = 0

    init {
        this.instaLogin = liker.username
        this.instaPassword = liker.password
        this.startPauseInterval = liker.startPauseInterval
        this.endPauseInterval = liker.endPauseInterval
        this.maxNumberOfLikes = liker.maxNumberOfLikes
        this.tagsSet = liker.tags
        this.proxyScheme = liker.proxy?.scheme?: ""
        this.proxyIp = liker.proxy?.ip?:""
        this.proxyPort = liker.proxy?.port?:-1
        this.proxyLogin = liker.proxy?.login?:""
        this.proxyPassword = liker.proxy?.password?:""
    }

    override fun run() {
        println("$accountName - ${Date()} Runnable Task with on thread ${Thread.currentThread().name}")
    }

    fun setParams(accountName: String) {
        this.accountName = accountName
    }

    suspend fun instaAutorization(): Instagram4j {

        var instagram = Instagram4j("none**", " none")

        if (proxyLogin.isNotEmpty() && proxyPassword.isNotEmpty() && proxyIp.isNotEmpty() && proxyPort > 0) {
            val proxy = HttpHost(proxyIp, proxyPort, proxyScheme)
            val credentialsProvider = BasicCredentialsProvider()
            credentialsProvider.setCredentials(
                AuthScope(AuthScope.ANY),
                UsernamePasswordCredentials(proxyLogin, proxyPassword)
            )

            instagram = Instagram4j.builder().username(instaLogin).password(instaPassword).proxy(proxy)
                .credentialsProvider(credentialsProvider).build()
            instagram.setup()
        } else if (proxyIp.isNotEmpty() && proxyPort > 0) {
            val proxy = HttpHost(proxyIp, proxyPort, proxyScheme)
            instagram = Instagram4j.builder().username(instaLogin).password(instaPassword).proxy(proxy).build()
            instagram.setup()
        } else if (instaLogin.isNotEmpty() && instaPassword.isNotEmpty()) {
            instagram = Instagram4j.builder().username(instaLogin).password(instaPassword).build()
            instagram.setup()
        } else {
            logger.info("No login or password... Program stopped")
        }
        return instagram
    }


    fun launcheHere() {

        val job = GlobalScope.launch(CoroutineName("myFirstNormCoroutine")) {
//            GlobalScope.launch(Dispatchers.Main) {
//                startButton.isDisable = true
//                stopButton.requestFocus()
//            }
            val pauseBetweenLikes = (startPauseInterval * 1000)..(endPauseInterval * 1000)
//            val maxNumberOfLikes = maxNumberOfLikesTextField.text.trim().toInt()

//            logTextArea.clear()
            rotatorCount = 1
//            val tagList = tagsTextArea.text.split("\n").filter { it.isNotBlank() }.toSet()
            /////////////////INIT////////////////
            if (tagsSet.isNotEmpty()) {
                // Login to instagram
//                val instagram = Instagram4j.builder().username(instaLogin).password(instaPassword).build()
                var instagram = Instagram4j("none**", " none")
                var instagramLoginResult = InstagramLoginResult()
                try {
                    instagram = instaAutorization()
                    instagramLoginResult = instagram.login()
                    if (instagramLoginResult.challenge != null) {
                        var instagramChallengeResult = instagram.sendRequest(InstagramChallengeRequest(instagramLoginResult.challenge.url, "choice=1"))
                        if (instagramChallengeResult.status == "200") {
                            var securityCode = readLine()
                            instagramChallengeResult = instagram.sendRequest(
                                InstagramChallengeRequest(
                                    instagramLoginResult.challenge.url,
                                    "security_code=$securityCode"
                                )
                            )
                        }
                        logger.info("Link: ${instagramLoginResult.challenge.url}")
                        logger.info("Api path: ${instagramLoginResult.challenge.api_path}")
                    }
                } catch (e: Exception) {
                    logger.info("Connection error")
//                    logRotatorInTextArea("Connection error")
                }

                if (instagramLoginResult.logged_in_user != null && instagramLoginResult.logged_in_user.username == instaLogin) {

                    val blacklistAccountsFile = File("${instaLogin}_blacklist.txt")
                    if (!Files.exists(blacklistAccountsFile.toPath())) {
                        Files.createFile(blacklistAccountsFile.toPath())
                    }
                    val blacklistPostsFile = File("${instaLogin}_blacklistPosts.txt")
                    if (!Files.exists(blacklistPostsFile.toPath())) {
                        Files.createFile(blacklistPostsFile.toPath())
                    }
                    val blackListAccounts = mutableSetOf<Long>()
                    val blacklistPosts = mutableSetOf<String>()

                    blackListAccounts.addAll(Files.readAllLines(blacklistAccountsFile.toPath()).map { it.toLong() })
                    blacklistPosts.addAll(Files.readAllLines(blacklistPostsFile.toPath()))

                    var likeCount = 0
                    while (true) {
                        for (tag in tagsSet) {
                            var postList: List<InstagramFeedItem> = listOf()
                            try {
                                val instagramFeedResult =
                                    getLastPostByTag(tag.tagName, instagram)/*?.items?: mutableListOf()*/
                                postList = instagramFeedResult?.items?.filter {
                                    !blacklistPosts.contains(it.code) && !blackListAccounts.contains(it.user.pk)
                                } ?: mutableListOf()
                                logger.info("Link count = ${postList.count()}")
//                                logRotatorInTextArea("Link count = ${postList.count()}")
                            } catch (e: Exception) {
                                e.printStackTrace()
                                val timeToSleep = (21000..31000L).random()
                                logger.info("We got bad page from instagram. Sleep $timeToSleep millisecond and try again.")
//                                logRotatorInTextArea("We got bad page from instagram. Sleep $timeToSleep millisecond and try again.")
                                delay(timeToSleep)
                            }
                            for (post in postList) {
                                val userId = post.user.pk
                                val shortcodePost = post.code
                                if (!blackListAccounts.contains(userId) && !blacklistPosts.contains(shortcodePost)) {
                                    try {
                                        val response = instagram.sendRequest(InstagramLikeRequest(post.pk))
                                        if (response.isSpam) {
                                            logger.info("Block action...")
//                                            logRotatorInTextArea("Block action...")
                                            delay(600000)
                                        }
                                        likeCount++
                                        logger.info("We made $likeCount likes.")
//                                        logRotatorInTextArea("We made $likeCount likes.")
                                        blackListAccounts.add(userId)
                                        blacklistPosts.add(shortcodePost)
                                        Files.write(
                                            Paths.get(blacklistAccountsFile.absolutePath),
                                            "$userId\n".toByteArray(),
                                            StandardOpenOption.APPEND
                                        )
                                        Files.write(
                                            Paths.get(blacklistPostsFile.absolutePath),
                                            "$shortcodePost\n".toByteArray(),
                                            StandardOpenOption.APPEND
                                        )
                                        logger.info("https://www.instagram.com/p/$shortcodePost/ got my like.")
//                                        logRotatorInTextArea("https://www.instagram.com/p/$shortcodePost/ got my like.")
                                        val timeToSleep = pauseBetweenLikes.random()
                                        logger.info("sleep between likes $timeToSleep")
//                                        logRotatorInTextArea("sleep between likes $timeToSleep")
                                        if (maxNumberOfLikes == likeCount) {
//                                            logRotatorInTextArea("We have ${maxNumberOfLikes}.")
                                            break
                                        } else if (response.message.equals("challenge_required", true)) {
                                            logger.info("challenge_required.")
//                                            logRotatorInTextArea("Challenge required.")
                                            break
                                        }
                                        delay(timeToSleep)//sleep after like
                                    } catch (e: Exception) {
                                        e.printStackTrace()
                                        val timeToSleep = (21000..31000L).random()
                                        logger.info("We got bad page from instagram. Sleep $timeToSleep millisecond and try again.")
//                                        logRotatorInTextArea("We got bad page from instagram. Sleep $timeToSleep millisecond and try again.")
                                        delay(timeToSleep)
                                    }
                                } else {
                                    logger.info("User id ${post.user.pk} in blacklist or post ${shortcodePost} already used!!!")
//                                    logRotatorInTextArea("User id ${post.user.pk} in blacklist or post ${shortcodePost} already used!!!")
                                    logger.info("https://www.instagram.com/p/$shortcodePost/ we do not like this post.")
//                                    logRotatorInTextArea("https://www.instagram.com/p/$shortcodePost/ we do not like this post.")
                                    val timeToSleep = (9000L..21000).random()
                                    delay(timeToSleep)
                                }
                            }
                            if (maxNumberOfLikes == likeCount) {
                                break
                            }
                            val timeToSleep = (9000L..21000).random()
                            logger.info("Sleep after each tag $timeToSleep millisec.")
//                            logRotatorInTextArea("Sleep after each tag $timeToSleep millisec.")
                            delay(timeToSleep)//sleep after each tag
                        }
                        if (maxNumberOfLikes == likeCount) {
                            break
                        }
                        val timeToSleep = (9000L..21000).random()
                        logger.info("Sleep before new tag request $timeToSleep millisec.")
//                        logRotatorInTextArea("Sleep before new tag request $timeToSleep millisec.")
                        delay(timeToSleep)//Sleep before new tag request
                    }
                } else {
                    logger.info("Login error.")
                }
            } else {
//                logRotatorInTextArea("User list is empty.")
            }
            logger.info("The program has completed.")
//            logRotatorInTextArea("The program has completed.")
//            GlobalScope.launch(Dispatchers.Main) {
//                startButton.isDisable = false
//            }
        }
//        jobs.add(job)
//        stopButton.onMouseClicked = EventHandler {
//            stopProgram(job) // cancel coroutine on click
//        }
    }

    fun getLastPostByTag(tag: String, instagram: Instagram4j): InstagramFeedResult? {
        val userResult = instagram.sendRequest(InstagramTagFeedRequest(tag))

        userResult.items.sortByDescending { it.taken_at }
//        userResult.items.forEach { println("https://www.instagram.com/p/${it.code}/") }
        return userResult
    }
}