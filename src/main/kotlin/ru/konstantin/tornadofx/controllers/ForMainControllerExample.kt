package ru.konstantin.tornadofx.controllers

import javafx.event.EventHandler
import javafx.scene.control.Button
import javafx.scene.control.ProgressIndicator
import javafx.scene.control.TextArea
import javafx.scene.control.TextField
import javafx.scene.layout.AnchorPane
import javafx.scene.layout.VBox
import kotlinx.coroutines.*
import org.apache.http.HttpHost
import org.apache.http.auth.AuthScope
import org.apache.http.auth.UsernamePasswordCredentials
import org.apache.http.impl.client.BasicCredentialsProvider
import org.brunocvcunha.instagram4j.Instagram4j
import org.brunocvcunha.instagram4j.requests.InstagramLikeRequest
import org.brunocvcunha.instagram4j.requests.InstagramTagFeedRequest
import org.brunocvcunha.instagram4j.requests.payload.InstagramFeedItem
import org.brunocvcunha.instagram4j.requests.payload.InstagramFeedResult
import org.brunocvcunha.instagram4j.requests.payload.InstagramLoginResult
import org.slf4j.LoggerFactory
import ru.konstantin.InstagramChallengeRequest
import tornadofx.View
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardOpenOption


class AccountEditView : View() {
    private var logger = LoggerFactory.getLogger(AccountEditView::class.java)

    override val root: AnchorPane by fxml("/fxml/addNewAccount.fxml")
    private val accountEditController: AccountEditController by inject()

    private val vboxMainPane: VBox by fxid("vboxMainPane")
    private val loginTextField: TextField by fxid("loginTextField")
    private val passwordTextField: TextField by fxid("passwordTextField")
    private val pauseBetweenLikesTextField: TextField by fxid("pauseBetweenLikes")
    private val maxNumberOfLikesTextField: TextField by fxid("maxNumberOfLikes")
    private val startButton: Button by fxid("saveButton")
    private val stopButton: Button by fxid("stopButton")
    private val tagsTextArea: TextArea by fxid("tagsSet")
    private val logTextArea: TextArea by fxid("logTextArea")
    private val proxyIpAndPortTextField: TextField by fxid("proxyIpAndPort")
    private val proxyLoginTextField: TextField by fxid("proxyLogin")
    private val proxyPasswordTextField: TextField by fxid("proxyPassword")
    private val progressIndicator: ProgressIndicator by fxid("progressIndicator")

    private val openWindowButton: Button by fxid("openWindowButton")

    private var rotatorCount = 1

//    private var jobs: MutableList<Job> = mutableListOf()

    private fun instaAutorization(): Instagram4j {

        var instagram = Instagram4j("none**", " none")
        val instaLogin = loginTextField.text.trim()
        val instaPassword = passwordTextField.text.trim()

        if (proxyLoginTextField.text.isNotEmpty() && proxyPasswordTextField.text.isNotEmpty() && proxyIpAndPortTextField.text.trim().isNotEmpty()) {
            val ip = proxyIpAndPortTextField.text.trim().substringBeforeLast(":").substringAfter("://").trim()
            val port = proxyIpAndPortTextField.text.trim().substringAfterLast(":")
            val proxylogin = proxyLoginTextField.text.trim()
            val proxyPassword = proxyPasswordTextField.text.trim()
            val scheme = proxyIpAndPortTextField.text.substringBefore("://")

            val proxy = HttpHost(ip, port.toInt(), scheme)
            val credentialsProvider = BasicCredentialsProvider()
            credentialsProvider.setCredentials(
                AuthScope(AuthScope.ANY),
                UsernamePasswordCredentials(proxylogin, proxyPassword)
            )

            instagram = Instagram4j.builder().username(instaLogin).password(instaPassword).proxy(proxy)
                .credentialsProvider(credentialsProvider).build()
            instagram.setup()
        } else if (proxyIpAndPortTextField.text.trim().isNotEmpty()) {
            val ip = proxyIpAndPortTextField.text.trim().substringBeforeLast(":").substringAfter("://").trim()
            val port = proxyIpAndPortTextField.text.trim().substringAfterLast(":").trim { it.equals("/") }
            val scheme = proxyIpAndPortTextField.text.substringBefore("://")

            val proxy = HttpHost(ip, port.toInt(), scheme)
            instagram = Instagram4j.builder().username(instaLogin).password(instaPassword).proxy(proxy).build()
            instagram.setup()
        } else if (loginTextField.text.trim().isNotEmpty() && passwordTextField.text.trim().isNotEmpty()) {
            instagram = Instagram4j.builder().username(instaLogin).password(instaPassword).build()
            instagram.setup()
        } else {
            logger.info("No login or password... Program stopped")
        }
        return instagram
    }


    fun launcheHere() {

        val job = GlobalScope.launch(CoroutineName("myFirstNormCoroutine")) {
            GlobalScope.launch(Dispatchers.Main) {
                                startButton.isDisable = true
                stopButton.requestFocus()
            }
            val pauseBetweenLikes =
                (pauseBetweenLikesTextField.text).split("..")[0].toLong() * 1000..pauseBetweenLikesTextField.text.split(
                    ".."
                )[1].toLong() * 1000
            val maxNumberOfLikes = maxNumberOfLikesTextField.text.trim().toInt()

            logTextArea.clear()
            rotatorCount = 1
            val tagList = tagsTextArea.text.split("\n").filter { it.isNotBlank() }.toSet()
            /////////////////INIT////////////////
            if (tagList.isNotEmpty()) {
                // Login to instagram
//                val instagram = Instagram4j.builder().username(instaLogin).password(instaPassword).build()
                var instagram = Instagram4j("none**", " none")
                var instagramLoginResult = InstagramLoginResult()
                try {
                    instagram = instaAutorization()
                    instagramLoginResult = instagram.login()
                    if (instagramLoginResult.challenge != null) {
                        var instagramChallengeResult = instagram.sendRequest(InstagramChallengeRequest(instagramLoginResult.challenge.url, "choice=1"))
                        if (instagramChallengeResult.status == "200") {
                            var securityCode = readLine()
                            instagramChallengeResult = instagram.sendRequest(
                                InstagramChallengeRequest(
                                    instagramLoginResult.challenge.url,
                                    "security_code=$securityCode"
                                )
                            )
                        }
                        logger.info("Link: ${instagramLoginResult.challenge.url}")
                        logger.info("Api path: ${instagramLoginResult.challenge.api_path}")
                    }
                } catch (e: Exception) {
                    logger.info("Connection error")
                    logRotatorInTextArea("Connection error")
                }

                if (instagramLoginResult.logged_in_user != null && instagramLoginResult.logged_in_user.username == loginTextField.text.trim()) {

                    val blacklistAccountsFile = File("${loginTextField.text.trim()}_blacklist.txt")
                    if (!Files.exists(blacklistAccountsFile.toPath())) {
                        Files.createFile(blacklistAccountsFile.toPath())
                    }
                    val blacklistPostsFile = File("${loginTextField.text.trim()}_blacklistPosts.txt")
                    if (!Files.exists(blacklistPostsFile.toPath())) {
                        Files.createFile(blacklistPostsFile.toPath())
                    }
                    val blackListAccounts = mutableSetOf<Long>()
                    val blacklistPosts = mutableSetOf<String>()

                    blackListAccounts.addAll(Files.readAllLines(blacklistAccountsFile.toPath()).map { it.toLong() })
                    blacklistPosts.addAll(Files.readAllLines(blacklistPostsFile.toPath()))

                    var likeCount = 0
                    while (true) {
                        for (tag in tagList) {
                            var postList: List<InstagramFeedItem> = listOf()
                            try {
                                val instagramFeedResult =
                                    getLastPostByTag(tag, instagram)/*?.items?: mutableListOf()*/
                                postList = instagramFeedResult?.items?.filter {
                                    !blacklistPosts.contains(it.code) && !blackListAccounts.contains(it.user.pk)
                                } ?: mutableListOf()
                                logger.info("Link count = ${postList.count()}")
                                logRotatorInTextArea("Link count = ${postList.count()}")
                            } catch (e: Exception) {
                                e.printStackTrace()
                                val timeToSleep = (21000..31000L).random()
                                logger.info("We got bad page from instagram. Sleep $timeToSleep millisecond and try again.")
                                logRotatorInTextArea("We got bad page from instagram. Sleep $timeToSleep millisecond and try again.")
                                delay(timeToSleep)
                            }
                            for (post in postList) {
                                val userId = post.user.pk
                                val shortcodePost = post.code
                                if (!blackListAccounts.contains(userId) && !blacklistPosts.contains(shortcodePost)) {
                                    try {
                                        val response = instagram.sendRequest(InstagramLikeRequest(post.pk))
                                        if (response.isSpam) {
                                            logger.info("Block action...")
                                            logRotatorInTextArea("Block action...")
                                            delay(600000)
                                        }
                                        likeCount++
                                        logger.info("We made $likeCount likes.")
                                        logRotatorInTextArea("We made $likeCount likes.")
                                        blackListAccounts.add(userId)
                                        blacklistPosts.add(shortcodePost)
                                        Files.write(
                                            Paths.get(blacklistAccountsFile.absolutePath),
                                            "$userId\n".toByteArray(),
                                            StandardOpenOption.APPEND
                                        )
                                        Files.write(
                                            Paths.get(blacklistPostsFile.absolutePath),
                                            "$shortcodePost\n".toByteArray(),
                                            StandardOpenOption.APPEND
                                        )
                                        logger.info("https://www.instagram.com/p/$shortcodePost/ got my like.")
                                        logRotatorInTextArea("https://www.instagram.com/p/$shortcodePost/ got my like.")
                                        val timeToSleep = pauseBetweenLikes.random()
                                        logger.info("sleep between likes $timeToSleep")
                                        logRotatorInTextArea("sleep between likes $timeToSleep")
                                        if (maxNumberOfLikesTextField.text.toInt() == likeCount) {
                                            logRotatorInTextArea("We have ${maxNumberOfLikesTextField.text.toInt()}.")
                                            break
                                        } else if (response.message.equals("challenge_required", true)) {
                                            logger.info("challenge_required.")
                                            logRotatorInTextArea("Challenge required.")
                                            break
                                        }
                                        delay(timeToSleep)//sleep after like
                                    } catch (e: Exception) {
                                        e.printStackTrace()
                                        val timeToSleep = (21000..31000L).random()
                                        logger.info("We got bad page from instagram. Sleep $timeToSleep millisecond and try again.")
                                        logRotatorInTextArea("We got bad page from instagram. Sleep $timeToSleep millisecond and try again.")
                                        delay(timeToSleep)
                                    }
                                } else {
                                    logger.info("User id ${post.user.pk} in blacklist or post ${shortcodePost} already used!!!")
                                    logRotatorInTextArea("User id ${post.user.pk} in blacklist or post ${shortcodePost} already used!!!")
                                    logger.info("https://www.instagram.com/p/$shortcodePost/ we do not like this post.")
                                    logRotatorInTextArea("https://www.instagram.com/p/$shortcodePost/ we do not like this post.")
                                    val timeToSleep = (9000L..21000).random()
                                    delay(timeToSleep)
                                }
                            }
                            if (maxNumberOfLikesTextField.text.toInt() == likeCount) {
                                break
                            }
                            val timeToSleep = (9000L..21000).random()
                            logger.info("Sleep after each tag $timeToSleep millisec.")
                            logRotatorInTextArea("Sleep after each tag $timeToSleep millisec.")
                            delay(timeToSleep)//sleep after each tag
                        }
                        if (maxNumberOfLikesTextField.text.toInt() == likeCount) {
                            break
                        }
                        val timeToSleep = (9000L..21000).random()
                        logger.info("Sleep before new tag request $timeToSleep millisec.")
                        logRotatorInTextArea("Sleep before new tag request $timeToSleep millisec.")
                        delay(timeToSleep)//Sleep before new tag request
                    }
                } else {
                    logger.info("Login error.")
                }
            } else {
                logRotatorInTextArea("User list is empty.")
            }
            logger.info("The program has completed.")
            logRotatorInTextArea("The program has completed.")
            GlobalScope.launch(Dispatchers.Main) {
                                startButton.isDisable = false
            }
        }
//        jobs.add(job)
        stopButton.onMouseClicked = EventHandler {
            stopProgram(job) // cancel coroutine on click
        }
    }

    fun stopProgram(job: Job) {
        GlobalScope.launch(Dispatchers.Main) {
            progressIndicator.isVisible = true
            vboxMainPane.isDisable = true
            startButton.isDisable = true
            job.cancel()
            while (job.isActive) {
                delay(500)
            }
            progressIndicator.isVisible = false
            vboxMainPane.isDisable = false
            startButton.isDisable = false
            logRotatorInTextArea("The stop button was pressed. Program stopped.")
        }
    }

    fun getLastPostByTag(tag: String, instagram: Instagram4j): InstagramFeedResult? {
        val userResult = instagram.sendRequest(InstagramTagFeedRequest(tag))

        userResult.items.sortByDescending { it.taken_at }
//        userResult.items.forEach { println("https://www.instagram.com/p/${it.code}/") }
        return userResult
    }

    private fun logRotatorInTextArea(str: String) {
        //Вычисляем количество строк в логе
        GlobalScope.launch(Dispatchers.Main) {
            val rotatorSize = 500
            val logList = logTextArea.text.split("\n")
            val cd = logList[0].length + 1
            if (logList.count() > rotatorSize) {
                logTextArea.deleteText(0, cd)
            }
            logTextArea.appendText("$rotatorCount. ")
            logTextArea.appendText("$str\n")
            rotatorCount++
        }
    }
}