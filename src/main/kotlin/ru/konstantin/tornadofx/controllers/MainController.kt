package ru.konstantin.tornadofx.controllers

import ru.konstantin.models.Liker
import ru.konstantin.repositories.LikerRepository
import tornadofx.Controller

class MainController : Controller() {
    private val likerRepository: LikerRepository by di()

    fun getAllLikers(): MutableList<Liker> {
        return likerRepository.findAll()

    }


}