package ru.konstantin.tornadofx.controllers

import org.slf4j.LoggerFactory
import ru.konstantin.models.Liker
import ru.konstantin.models.Proxy
import ru.konstantin.models.Tag
import ru.konstantin.repositories.LikerRepository
import ru.konstantin.repositories.TagRepository
import ru.konstantin.tornadofx.views.AccountEditView
import tornadofx.Controller

class AccountEditController: Controller() {
    private var logger = LoggerFactory.getLogger(AccountEditController::class.java)

    private val tagRepository: TagRepository by di()
    private val likerRepository: LikerRepository by di()
    private val accountEditView: AccountEditView by inject()
//    private val mainView: MainView by inject()

//    fun writeAccountToDb(liker: Liker): Liker {
////        var liker = Liker(username = username, password = password, proxy = proxy, tags = tags)
//        return likerService.saveAccountIntoDB(liker)
////        println("Writing ${liker.username} to database!")
//    }

    fun createNewOrEditAccount() {

//        var selectedItem = mainView.mainTable.getSelectionModel().getSelectedItems().first()

        val login = accountEditView.loginTextField.text.trim()
        val password = accountEditView.passwordTextField.text.trim()
        val startPauseInterval = accountEditView.pauseBetweenLikesTextField.text.trim().split("..")[0].toLong()
        val endPauseInterval = accountEditView.pauseBetweenLikesTextField.text.trim().split("..")[1].toLong()
        val maxNumberOfLikes = accountEditView.maxNumberOfLikesTextField.text.trim().toInt()
        val cron = accountEditView.cronTextField.text.trim()
        val proxySource = accountEditView.proxyIpAndPortTextField.text.trim()
        val proxyLogin = accountEditView.proxyLoginTextField.text.trim()
        val proxyPassword = accountEditView.proxyPasswordTextField.text.trim()
        val tagsStringSet = accountEditView.tagsTextArea.text.split("\n").toSet()
        val proxy = Proxy()

        if (proxySource.isNotEmpty()) {
            proxy.scheme = proxySource.substringBefore("://")
            proxy.ip = proxySource.substringAfter("://").substringBefore(":")
            proxy.port = proxySource.substringAfterLast(":").toInt()
            if (proxyLogin.isNotEmpty() && proxyPassword.isNotEmpty()) {
                proxy.login = proxyLogin
                proxy.password = proxyPassword
            }
        }

        var tags = mutableSetOf<Tag>()
        var liker = Liker()

        liker.proxy = proxy

        liker.username = login
        liker.password = password
        liker.startPauseInterval = startPauseInterval
        liker.endPauseInterval = endPauseInterval
        liker.maxNumberOfLikes = maxNumberOfLikes
        liker.cron = cron
        liker = likerRepository.save(liker)

        tagsStringSet.forEach {
            if (it.isNotBlank()) {
                var tag: Tag? = tagRepository.getTagByLikerrrAndTagName(liker, it)
                if (tag == null) {
                    tag = Tag()
                    tag.tagName = it
//                    tag.likerrr = liker
                    tag = tagRepository.save(tag)
                }
                tag?.let { it1 -> tags.add(it1) }
            }
        }

//        tags.map {
//            it.likerrr = liker
//            tagRepository.save(it)
//        }

        liker.tags = tagRepository.saveAll(tags).toMutableSet()
        liker = likerRepository.save(liker)
//        likerService.saveAccountIntoDB(liker, tags)
//        tagRepository.saveAll(tags)

//        liker = likerService.saveAccountIntoDB(liker)

        }



}