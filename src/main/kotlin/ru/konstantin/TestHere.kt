//import org.springframework.kotlin.coroutine.EnableCoroutine
import javafx.application.Application
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.context.annotation.ComponentScan
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.scheduling.annotation.EnableScheduling
import ru.konstantin.tornadofx.views.MainView
import tornadofx.App
import tornadofx.DIContainer
import tornadofx.FX
import kotlin.reflect.KClass

@SpringBootApplication
@EnableJpaRepositories(basePackages = arrayOf("ru.konstantin"))
@ComponentScan(basePackages = arrayOf("ru.konstantin"))
@EnableScheduling
@EntityScan("ru.konstantin")
//@EnableCoroutine(
//    proxyTargetClass = false, mode = AdviceMode.PROXY,
//    order = Ordered.LOWEST_PRECEDENCE, schedulerDispatcher = "")
class TestHere : App(MainView::class) {

    private lateinit var context: ConfigurableApplicationContext

    override fun init() {
        this.context = SpringApplication.run(this.javaClass)
        context.autowireCapableBeanFactory.autowireBean(this)
        FX.dicontainer = object : DIContainer {
            override fun <T : Any> getInstance(type: KClass<T>): T = context.getBean(type.java)
            override fun <T : Any> getInstance(type: KClass<T>, name: String): T = context.getBean(type.java,name)
        }
    }

    override fun stop() {
        super.stop()
        context.close()
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            launch(TestHere::class.java, *args)
        }
    }
}