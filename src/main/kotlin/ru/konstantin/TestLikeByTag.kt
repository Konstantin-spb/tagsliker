package ru.konstantin

import org.apache.log4j.Logger
import org.brunocvcunha.instagram4j.Instagram4j
import org.brunocvcunha.instagram4j.requests.InstagramTagFeedRequest
import org.brunocvcunha.instagram4j.requests.payload.InstagramFeedResult

class TestLikeByTag {
    private val logger = Logger.getLogger(TestLikeByTag::class.java)

    fun startHere() {
//        // Login to instagram
//        val instagram = Instagram4j.builder().username("kiranoskova240").password("Itisagooddaytofun232323").build()
//        instagram.setup()
//        val instagramLoginResult = instagram.login()
//        if (instagramLoginResult.challenge != null) {
//            logger.info("Link: ${instagramLoginResult.challenge.url}")
//            logger.info("Api path: ${instagramLoginResult.challenge.api_path}")
//        }
//
//        val blacklistAccountsFile = File("blacklist.txt")
//        val blacklistPostsFile = File("blacklistPosts.txt")
//        val blackListAccounts = mutableSetOf<Long>()
//        val blacklistPosts = mutableSetOf<String>()
//
//        blackListAccounts.addAll(Files.readAllLines(blacklistAccountsFile.toPath()).map { it.toLong() })
//        blacklistPosts.addAll(Files.readAllLines(blacklistPostsFile.toPath()))
//
//        val testLikeByTag = ru.konstantin.TestLikeByTag()
//        var tagList = mutableListOf<String>(
//            "macrophoto",
//            "macrophotos",
//            "macrophotographylove",
//            "macrophotografy",
//            "photonaturemacro",
//            "macrophotographer"
//        )
////        tagList.addAll()
//        var likeCount = 0
//        while (true) {
//            for (tag in tagList) {
//                var postList: List<InstagramFeedItem> = listOf()
//                try {
//                    val instagramFeedResult =
//                        testLikeByTag.getLastPostByTag(tag, instagram)/*?.items?: mutableListOf()*/
//                    postList = instagramFeedResult?.items?.filter {
//                        !blacklistPosts.contains(it.code) && !blackListAccounts.contains(it.user.pk)
//                    } ?: mutableListOf()
//                    logger.info("Link count = ${postList.count()}")
//                } catch (e: Exception) {
//                    e.printStackTrace()
//                    val timeToSleep = (21000..31000L).random()
//                    logger.info("We got bad page from instagram. Sleep $timeToSleep millisecond and try again.")
//                    Thread.sleep(timeToSleep)
//                }
//                for (post in postList) {
//                    val userId = post.user.pk
//                    val shortcodePost = post.code
//                    if (!blackListAccounts.contains(userId) && !blacklistPosts.contains(shortcodePost)) {
//                        try {
//                            val response = instagram.sendRequest(InstagramLikeRequest(post.pk))
//                            if (response.isSpam && likeCount == 1600) {
//                                logger.info("Block...")
//                                return
//                            }
//                            likeCount++
//                            logger.info("We made $likeCount likes.")
//                            blackListAccounts.add(userId)
//                            blacklistPosts.add(shortcodePost)
//                            Files.write(
//                                Paths.get(blacklistAccountsFile.absolutePath),
//                                "$userId\n".toByteArray(),
//                                StandardOpenOption.APPEND
//                            )
//                            Files.write(
//                                Paths.get(blacklistPostsFile.absolutePath),
//                                "$shortcodePost\n".toByteArray(),
//                                StandardOpenOption.APPEND
//                            )
//                            logger.info("https://www.instagram.com/p/$shortcodePost/ got my like.")
//                            val timeToSleep = (10000L..21000).random()
//                            logger.info("sleep between likes $timeToSleep")
//                            Thread.sleep(timeToSleep)//sleep after like
//                        } catch (e: Exception) {
//                            e.printStackTrace()
//                            val timeToSleep = (21000..31000L).random()
//                            logger.info("We got bad page from instagram. Sleep $timeToSleep millisecond and try again.")
//                            Thread.sleep(timeToSleep)
//                        }
//                    } else {
//                        logger.info("User id ${post.user.pk} in blacklist or post ${shortcodePost} already used!!!")
//                        logger.info("https://www.instagram.com/p/$shortcodePost/ we do not like this post.")
//                    }
//                }
//                val timeToSleep = (9000L..21000).random()
//                logger.info("sleep after each tag $timeToSleep millisec.")
//                Thread.sleep(timeToSleep)//sleep after each tag
//            }
//            val timeToSleep = (9000L..21000).random()
//            logger.info("Sleep before new tag request $timeToSleep millisec.")
//            Thread.sleep(timeToSleep)//Sleep before new tag request
//        }
    }

    fun getLastPostByTag(tag: String, instagram: Instagram4j): InstagramFeedResult? {
        val userResult = instagram.sendRequest(InstagramTagFeedRequest(tag))

        userResult.items.sortByDescending { it.taken_at }
//        userResult.items.forEach { println("https://www.instagram.com/p/${it.code}/") }
        return userResult
    }
}

fun main() {
    TestLikeByTag().startHere()
}