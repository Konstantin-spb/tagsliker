package ru.konstantin

import javafx.stage.Stage
import ru.konstantin.tornadofx.views.MainView
import tornadofx.App
import tornadofx.launch

class MyApp: App() {
    override val primaryView = MainView::class

    override fun start(stage: Stage) {
        super.start(stage)
        stage.width = 1000.0
        stage.height = 700.0
        stage.isResizable = false
    }
}

fun main(args: Array<String>) {
    launch<MyApp>(args)
}