package ru.konstantin

//import org.springframework.kotlin.coroutine.EnableCoroutine
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler

@SpringBootApplication
//@EnableScheduling
//@EnableCoroutine(
//    proxyTargetClass = false, mode = AdviceMode.PROXY,
//    order = LOWEST_PRECEDENCE, schedulerDispatcher = "")
class SpringStart {

//    @Bean(name = ["MyPool"])
//    fun threadPoolTaskScheduler(): ThreadPoolTaskScheduler {
//        val threadPoolTaskScheduler = ThreadPoolTaskScheduler()
//        threadPoolTaskScheduler.poolSize = 1000
//        return threadPoolTaskScheduler
//    }
}

suspend fun main(args: Array<String>) {
    val context = runApplication<SpringStart>(*args)
//    var testBean = context.getBean(LikerActions::class.java)
//    testBean.startHere()
}

